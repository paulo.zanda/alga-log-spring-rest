package com.algawork.algaLog_api.common;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ModelMapperConfig {

    /**
     * esta class informa ao Spring que esta é uma classe de configuracao
     * **/
    @Bean //indica que este metodo instancia e configura o bean que sera gerenciado pelo spring e permite
          //a ingessao em outros classes
    public ModelMapper modelMapper(){
        return new ModelMapper();
    }
}
