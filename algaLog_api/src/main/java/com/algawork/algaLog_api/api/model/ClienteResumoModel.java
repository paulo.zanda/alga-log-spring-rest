package com.algawork.algaLog_api.api.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClienteResumoModel {

    private Long Id;
    private String nome;
}
