package com.algawork.algaLog_api.api.controller;

import com.algawork.algaLog_api.api.assembler.OcorrenciaAssembler;
import com.algawork.algaLog_api.api.model.OcorrenciaModel;
import com.algawork.algaLog_api.api.model.input.OcorrenciaInput;
import com.algawork.algaLog_api.domain.model.Entrega;
import com.algawork.algaLog_api.domain.model.Ocorrencia;
import com.algawork.algaLog_api.domain.service.BuscaEntregaService;
import com.algawork.algaLog_api.domain.service.RegistoOcorrenciaService;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/entregas/{entregaId}/ocorrencia")
public class OcorrenciaController {

    private RegistoOcorrenciaService registoOcorrenciaService;
    private OcorrenciaAssembler ocorrenciaAssembler;
    private BuscaEntregaService buscaEntregaService;
    /*
    * TEM BUG DE - Internal Server Error*/
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public OcorrenciaModel registarOcorrencia(@PathVariable Long entregaId,
                                              @Valid @RequestBody OcorrenciaInput ocorrenciaInput){
        Ocorrencia ocorrencia = registoOcorrenciaService
                .registarOcorrencia(entregaId, ocorrenciaInput.getDescricao());

        return ocorrenciaAssembler.toModel(ocorrencia);


    }

    @GetMapping
    public List<OcorrenciaModel> listar(@PathVariable Long entregaId){
        Entrega entrega = buscaEntregaService.buscar(entregaId);

        return ocorrenciaAssembler.toColletionModel(entrega.getOcorrenciaList());
    }
}
