package com.algawork.algaLog_api.api.controller;

import com.algawork.algaLog_api.api.assembler.EntregaAssembler;
import com.algawork.algaLog_api.api.model.DestinatarioModel;
import com.algawork.algaLog_api.api.model.EntregaModel;
import com.algawork.algaLog_api.api.model.input.EntregaInput;
import com.algawork.algaLog_api.domain.model.Entrega;
import com.algawork.algaLog_api.domain.repository.EntregaRepository;
import com.algawork.algaLog_api.domain.service.FinalizacaoEntregaService;
import com.algawork.algaLog_api.domain.service.SolicitacaoEntregaService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/entregas")
public class EntregaController {

    private SolicitacaoEntregaService solicitacaoEntregaService;
    private EntregaRepository entregaRepository;
    private EntregaAssembler assembler;
    private FinalizacaoEntregaService finalizacaoEntregaService;

    @PostMapping("/solicitar")
    @ResponseStatus(HttpStatus.CREATED)
    public EntregaModel solicitar(@Valid @RequestBody EntregaInput entregaInput){

        Entrega entrega = assembler.toEntity(entregaInput);
        return assembler.toModel(solicitacaoEntregaService.solicitar(entrega));

    }

    @GetMapping("/listar")
    public List<EntregaModel> listar(){
        return assembler.toColletionModel(entregaRepository.findAll());
    }

    @GetMapping("/buscar/{entregaId}")
    public ResponseEntity<EntregaModel> buscar(@PathVariable Long entregaId){
        /**return entregaRepository.findById(entregaId)
        .map(entrega -> {

                     * ou podemos utilizar o ModelMapper para preencher a instancia do mapper
                     *
                    EntregaModel entregaModel = modelMapper.map(entrega,EntregaModel.class);

                   /* EntregaModel entregaModel = new EntregaModel();
                    entregaModel.setId(entrega.getId());
                    entregaModel.setNomeCliente(entrega.getCliente().getNome());

                    entregaModel.setDestinatario(new DestinatarioModel());
                        entregaModel.getDestinatario().setNome(entrega.getDestinatario().getNome());
                        entregaModel.getDestinatario().setLogradouro(entrega.getDestinatario().getLogradouro());
                        entregaModel.getDestinatario().setNumero(entrega.getDestinatario().getNumero());
                        entregaModel.getDestinatario().setComplemento(entrega.getDestinatario().getComplemento());
                        entregaModel.getDestinatario().setBairro(entrega.getDestinatario().getBairro());

                    entregaModel.setTaxa(entrega.getTaxa());
                    entregaModel.setStatus(entrega.getStatusEntrega());
                    entregaModel.setDataPedido(entrega.getDataPedido());
                    entregaModel.setDataFinalizacao(entrega.getDataFinalizacao());

                    return ResponseEntity.ok(entregaModel);
                }).orElse(ResponseEntity.notFound().build());*/

        return entregaRepository.findById(entregaId)
                .map(entrega -> ResponseEntity.ok(assembler.toModel(entrega)))
                .orElse(ResponseEntity.notFound().build());
        /*
        * USANDO DTO*/
    }

    @PutMapping("/{entregaId}/finalizacao")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void finalizar(@PathVariable Long entregaId){
        finalizacaoEntregaService.finalizar(entregaId);
    }

}
