package com.algawork.algaLog_api.api.model.input;

import com.algawork.algaLog_api.domain.model.Destinatario;
import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.OffsetDateTime;

@Getter
@Setter
public class EntregaInput {

    @Valid
    @NotNull
    private CLienteIdInput cliente;

    @Valid
    @NotNull
    private DestinatarioInput destinatarioInput;

    @NotNull
    private BigDecimal taxa;
}
