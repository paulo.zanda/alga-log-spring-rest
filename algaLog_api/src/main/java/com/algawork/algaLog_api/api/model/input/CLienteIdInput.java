package com.algawork.algaLog_api.api.model.input;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CLienteIdInput {

    @NotNull
    private Long id;
}
