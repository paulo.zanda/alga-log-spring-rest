package com.algawork.algaLog_api.api.controller;

import com.algawork.algaLog_api.domain.repository.ClienteRepository;
import com.algawork.algaLog_api.domain.model.Cliente;
import com.algawork.algaLog_api.domain.service.CatalogoClienteService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/clientes")
public class ClienteController {

    @PersistenceContext
    private EntityManager entityManager;
    private ClienteRepository clienteRepository;
    private CatalogoClienteService catalogoClienteService;
    //private ClienteAssembler assembler;

    @GetMapping("/clientesListar")
    public List<Cliente> listar(){

        return clienteRepository.findAll();
    }

    /*@GetMapping("/clientesListarNome")
    public List<Cliente> listarPorNome(){

        return clienteRepository.findByNomeContaining("m");
    }*/
    @GetMapping("/listarClientes")
    public List<Cliente> listarCliente(){
        return entityManager.createQuery("from Cliente",Cliente.class).getResultList();
        /*
         * ESTA É OUTRA FORMA DE CRIAR UM METOFO GET.*/
    }

    @GetMapping("/encotrar/{clienteId}")
    public ResponseEntity<Cliente> buscarCliente(@PathVariable Long clienteId){

       /* Optional <Cliente> cliente = clienteRepository.findById(clienteId);
        if(cliente.isPresent()) {
            return ResponseEntity.ok(cliente.get());
        }
        return ResponseEntity.notFound().build();

        OU POSSO USAR UMA EXPRESSION
        */
        return clienteRepository.findById(clienteId)
                //.map(cliente -> ResponseEntity.ok(cliente)) or
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping("/adicionarCliente")
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente adicionar(@Valid @RequestBody Cliente cliente){
        //return clienteRepository.save(cliente);
        //Cliente cliente = assembler.toEntity(clienteInput);
//        return assembler.toModel(catalogoClienteService.salvar(cliente));
        return catalogoClienteService.salvar(cliente);
    }

    @PutMapping("/atualizarCliente/{clienteId}")
    public ResponseEntity<Cliente> atualizar(@Valid @PathVariable Long clienteId, @RequestBody Cliente cliente){
        if(!clienteRepository.existsById(clienteId)){
            return ResponseEntity.notFound().build();
        }
        cliente.setId(clienteId);
        //return ResponseEntity.ok(clienteRepository.save(cliente));
        return ResponseEntity.ok(catalogoClienteService.salvar(cliente));
    }

    @DeleteMapping("apagar/{clienteId}")
    public ResponseEntity<Void> apagar(@PathVariable Long clienteId){
        if(!clienteRepository.existsById(clienteId)){
            return ResponseEntity.notFound().build();
        }
        //clienteRepository.deleteById(clienteId);
        catalogoClienteService.excluirCliente(clienteId);
        return ResponseEntity.noContent().build();
    }
}
