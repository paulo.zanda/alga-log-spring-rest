package com.algawork.algaLog_api.api.assembler;

import com.algawork.algaLog_api.api.model.OcorrenciaModel;
import com.algawork.algaLog_api.domain.model.Ocorrencia;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class OcorrenciaAssembler {

    private ModelMapper mapper;

    public OcorrenciaModel toModel(Ocorrencia ocorrencia){
        return mapper.map(ocorrencia,OcorrenciaModel.class);
    }

    public List<OcorrenciaModel> toColletionModel(List<Ocorrencia> ocorrenciaList){
        return ocorrenciaList.stream()
                .map(this::toModel)
                .collect(Collectors.toList());
    }
}
