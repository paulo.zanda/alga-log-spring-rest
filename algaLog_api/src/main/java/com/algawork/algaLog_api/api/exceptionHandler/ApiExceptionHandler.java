package com.algawork.algaLog_api.api.exceptionHandler;

import com.algawork.algaLog_api.domain.exception.EntidadeNaoEncontradaException;
import com.algawork.algaLog_api.domain.exception.NegocioException;
import lombok.AllArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    private MessageSource messageSource;

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status,
                                                                  WebRequest request) {
        /*TRAMENTO DE ERRO*/
        List<Problema.Campo> campos = new ArrayList<>();
        Problema problema = new Problema();

        for (ObjectError error :ex.getBindingResult().getAllErrors()){
            String nome = ((FieldError) error).getField();
            String mensangem = messageSource.getMessage(error, LocaleContextHolder.getLocale());

            campos.add(new Problema.Campo(nome,mensangem));
        }

        problema.setStatus(status.value());
        problema.setDateTime(OffsetDateTime.now());
        problema.setTitulo("UM OU MAIS CAMPOS ESTAO INVALIDO. FACA O PREENCHIMENTO CORRETO E TENTE NOVAMENTE");
        problema.setCampos(campos);

        return handleExceptionInternal(ex,problema,headers,status,request);
    }

    @ExceptionHandler(EntidadeNaoEncontradaException.class)
    public ResponseEntity<Object> handleEntidadeNaoEncontrada(EntidadeNaoEncontradaException ex, WebRequest request){

        Problema problema = new Problema();
        HttpStatus status = HttpStatus.NOT_FOUND;

        problema.setStatus(status.value());
        problema.setDateTime(OffsetDateTime.now());
        problema.setTitulo(ex.getMessage());

        return handleExceptionInternal(ex,problema,new HttpHeaders(),status,request);
    }


    @ExceptionHandler(NegocioException.class)
    public ResponseEntity<Object> handleNegocio(NegocioException ex, WebRequest request){

        Problema problema = new Problema();
        HttpStatus status = HttpStatus.BAD_REQUEST;

        problema.setStatus(status.value());
        problema.setDateTime(OffsetDateTime.now());
        problema.setTitulo(ex.getMessage());

        return handleExceptionInternal(ex,problema,new HttpHeaders(),status,request);
    }

}
