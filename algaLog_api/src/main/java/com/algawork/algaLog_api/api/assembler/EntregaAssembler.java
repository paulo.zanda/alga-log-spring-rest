package com.algawork.algaLog_api.api.assembler;

import com.algawork.algaLog_api.api.model.EntregaModel;
import com.algawork.algaLog_api.api.model.input.EntregaInput;
import com.algawork.algaLog_api.domain.model.Entrega;
import lombok.AllArgsConstructor;
import net.bytebuddy.asm.Advice;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Component
public class EntregaAssembler {

    /*
    * ESTA CLASSE É UMA CLASSE MONTADORA PARA QUE A
    * CLASSE EntregaController NAO SEJA TOTALMENTE
    * DEPENDENTE DA CLASSE ModelMapper.
    *
    * ESTA CLASSE VAI ENTREGAR UM SERVIÇO A CLASSE
    * */
    private ModelMapper mapper;

    public EntregaModel toModel(Entrega entrega){
        return mapper.map(entrega,EntregaModel.class);
    }

    public List<EntregaModel> toColletionModel(List<Entrega> entregas){
        return entregas.stream()
                .map(this::toModel)
                .collect(Collectors.toList());
    }
    public Entrega toEntity(EntregaInput entregaInput){
        return mapper.map(entregaInput,Entrega.class);
    }
}
