package com.algawork.algaLog_api.domain.model;

import com.algawork.algaLog_api.domain.ValidationGroups;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Objects;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@AllArgsConstructor
@Entity
@Table(name = "clientes")
public class Cliente {


    @NotNull(groups = ValidationGroups.CLienteId.class)
    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nome")
    @NotBlank
    @Size(max = 50)
    private String nome;

    @Column(name = "email")
    @NotBlank
    @Email
    @Size(max = 50)
    private String email;

    @NotBlank //Bean validation
    @Size(max = 50)
    private String telefone;

    public Cliente (){
        this.id = 0L;
        this.nome ="";
        this.email = "";
        this.telefone ="";
    }
    /* PODE SER FEITO UTILZANDO NOTACAO DO LOMBOK --> veja acima
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Cliente)) return false;
        Cliente cliente = (Cliente) o;
        return id.equals(cliente.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
    */

}
