package com.algawork.algaLog_api.domain.service;

import com.algawork.algaLog_api.domain.exception.EntidadeNaoEncontradaException;
import com.algawork.algaLog_api.domain.exception.NegocioException;
import com.algawork.algaLog_api.domain.model.Entrega;
import com.algawork.algaLog_api.domain.repository.EntregaRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class BuscaEntregaService {

    private EntregaRepository entregaRepository;

    public Entrega buscar(Long entregaId){
        return entregaRepository.findById(entregaId)
                .orElseThrow(()-> new EntidadeNaoEncontradaException("ENTREGA NAO ENCONTRADA"));
    }
}
