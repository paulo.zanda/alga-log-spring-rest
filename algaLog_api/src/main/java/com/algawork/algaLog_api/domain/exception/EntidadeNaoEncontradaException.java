package com.algawork.algaLog_api.domain.exception;

public class EntidadeNaoEncontradaException extends NegocioException {

    private static final long serialVersionID = 1L;

    public EntidadeNaoEncontradaException(String message) {
        super(message);
    }
}
