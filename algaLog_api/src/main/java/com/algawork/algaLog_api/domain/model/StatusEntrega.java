package com.algawork.algaLog_api.domain.model;

public enum StatusEntrega {

    PENDENTE, FINALIZADA, CANCELADA
}
