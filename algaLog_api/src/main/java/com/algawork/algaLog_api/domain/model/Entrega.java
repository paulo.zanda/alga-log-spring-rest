package com.algawork.algaLog_api.domain.model;

import com.algawork.algaLog_api.domain.ValidationGroups;
import com.algawork.algaLog_api.domain.exception.NegocioException;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.groups.ConvertGroup;
import javax.validation.groups.Default;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Entrega {


    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Valid
    @ConvertGroup(from= Default.class, to= ValidationGroups.CLienteId.class) //isto vai validar apenas o ID para cliente como para a entrega apenas
    @NotNull
    @ManyToOne //isso dita o relacionamento entre duas tabelas (Entrega e cliente)
    //@JoinColumn(name = "cliente_id") //fazendo o mapeamento da chave estrangeira
    private Cliente cliente;

    @Valid
    @NotNull
    @Embedded
    private Destinatario destinatario;

    @NotNull
    private BigDecimal taxa;


    @OneToMany(mappedBy = "entrega",cascade = CascadeType.ALL)
    private List<Ocorrencia> ocorrenciaList = new ArrayList<>();

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private StatusEntrega statusEntrega;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private OffsetDateTime dataPedido;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private OffsetDateTime dataFinalizacao;

    /*Metodo para preencher a lista de ocorrencias*/
    public Ocorrencia adicionarOcorrencia(String descricao) {
        Ocorrencia ocorrencia = new Ocorrencia();

        ocorrencia.setDescricao(descricao);
        ocorrencia.setDataRegisto(OffsetDateTime.now());
        ocorrencia.setEntrega(this);

        this.getOcorrenciaList().add(ocorrencia);
        return ocorrencia;
    }

    public void finalizar() {
        if(naoPodeSerFinalizada()){
            throw new NegocioException("ENTREGA NAO PODE SER FINALIZADA");
        }
        setStatusEntrega(StatusEntrega.FINALIZADA);
        setDataFinalizacao(OffsetDateTime.now());
    }

    public boolean podeSerFinalida(){
        return StatusEntrega.PENDENTE.equals(getStatusEntrega());
    }

    public boolean naoPodeSerFinalizada(){
        return !podeSerFinalida();
    }
}
