package com.algawork.algaLog_api.domain.service;

import com.algawork.algaLog_api.domain.model.Entrega;
import com.algawork.algaLog_api.domain.model.StatusEntrega;
import com.algawork.algaLog_api.domain.repository.EntregaRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.TransactionScoped;
import javax.transaction.Transactional;

@AllArgsConstructor
@Service
public class FinalizacaoEntregaService {

    private BuscaEntregaService buscaEntregaService;

    @Transactional
    public void finalizar(Long entregaId){
        Entrega entrega = buscaEntregaService.buscar(entregaId);

       entrega.finalizar();
    }
}
