package com.algawork.algaLog_api.domain.service;

import com.algawork.algaLog_api.domain.exception.NegocioException;
import com.algawork.algaLog_api.domain.model.Cliente;
import com.algawork.algaLog_api.domain.repository.ClienteRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@AllArgsConstructor
@Service
public class CatalogoClienteService {
/*
*Esta classe existe por causa das regras do negocio. todas as alteracoes de cadastro, atualizacao, exclusao
* entre outras regras devem ser escritas aqui
*.
* */
    private ClienteRepository clienteRepository;

    @Transactional
    public Cliente salvar(Cliente cliente){
        //utilizando stream do java e expression
        boolean emailEmUso = clienteRepository.findByEmail(cliente.getEmail())
                .stream()
                .anyMatch(clienteExistene -> !clienteExistene.equals(cliente));

        if(emailEmUso){
            throw new NegocioException("JA EXISTE UM CLIENTE CADASTRADO COM ESTE EMAIL");
        }
        return clienteRepository.save(cliente);

    }

    @Transactional
    public void excluirCliente(Long clienteId){
        clienteRepository.deleteById(clienteId);
    }

    @Transactional
    public Cliente buscar(Long clienteId){
        return clienteRepository.findById(clienteId)
                .orElseThrow(()-> new NegocioException("CLIENTE NAO ENCONTRADO"));
    }
}
