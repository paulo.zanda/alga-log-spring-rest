package com.algawork.algaLog_api.domain.service;

import com.algawork.algaLog_api.domain.exception.NegocioException;
import com.algawork.algaLog_api.domain.model.Entrega;
import com.algawork.algaLog_api.domain.model.Ocorrencia;
import com.algawork.algaLog_api.domain.repository.EntregaRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@AllArgsConstructor
@Service
public class RegistoOcorrenciaService {


    private BuscaEntregaService buscaEntregaService;

    @Transactional
    public Ocorrencia registarOcorrencia(Long entregaId, String descicao){

        Entrega entrega = buscaEntregaService.buscar(entregaId);

        return entrega.adicionarOcorrencia(descicao);
    }
}
