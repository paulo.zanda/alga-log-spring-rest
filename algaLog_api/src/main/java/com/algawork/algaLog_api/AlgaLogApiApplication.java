package com.algawork.algaLog_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class  AlgaLogApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlgaLogApiApplication.class, args);
	}

}
