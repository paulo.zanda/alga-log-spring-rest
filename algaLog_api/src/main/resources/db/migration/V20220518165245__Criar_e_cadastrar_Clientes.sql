create table clientes(
    id int(5) not null auto_increment,
    nome varchar(50) not null,
    email varchar(50) not null,
    telefone varchar(50) not null,
    primary key (id)
)ENGINE=innoDB DEFAULT CHARSET=utf8;

insert into clientes (nome,email,telefone) values ('Joao Jose','j.jose@teste.com','845569253');
insert into clientes (nome,email,telefone) values ('Maria Ana','m.ana@teste.com','859632560');
insert into clientes (nome,email,telefone) values ('Paula Joao','p.joao@teste.com','869587632');
insert into clientes (nome,email,telefone) values ('Manuela Santos','m.santos@teste.com','879563210');