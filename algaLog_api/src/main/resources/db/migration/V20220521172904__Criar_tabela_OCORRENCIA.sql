create table ocorrencia (
    id bigint not null auto_increment,
    entrega_id bigint not null,
    descricao text not null,
    dataRegisto datetime not null,

    primary key (id)
)ENGINE=innoDB DEFAULT CHARSET=utf8;

alter table ocorrencia add constraint fk_ocorrencia_entrega
foreign key (entrega_id) references entrega(id);